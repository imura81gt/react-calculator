import React from 'react';

import './App.css';
import NumberButton from './components/NumberButton';
import OperatorButton from './components/OperatorButton';
import Result from './components/Result';

function App() {
  return (
    <React.Fragment>
      <div className="result">
        <Result result={0} />
      </div>

      <div className="wrapper">
        <div className="number">
        <div className="numberUpper">
            <NumberButton n={7} />
            <NumberButton n={8} />
            <NumberButton n={9} />
          </div>
          <div className="numberMiddle">
            <NumberButton n={4} />
            <NumberButton n={5} />
            <NumberButton n={6} />
          </div>
          <div className="numberLower">
            <NumberButton n={1} />
            <NumberButton n={2} />
            <NumberButton n={3} />
          </div>
          <div className="numberZero">
            <NumberButton n={0} />
            <span className="allClear">
              <OperatorButton o={'AC'} />
            </span>
            <span className="equal">
              <OperatorButton o={'='} />
            </span>
          </div>
        </div>
        <div className="operator">
        <OperatorButton o={'÷'} />
        <OperatorButton o={'×'} />
        <OperatorButton o={'+'} />
        <OperatorButton o={'-'} />
        </div>
      </div>
    </React.Fragment>

  );
}

export default App;
