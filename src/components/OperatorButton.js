import React from 'react';

const OperatorButton = ({o}) => <button>{o}</button>

export default OperatorButton;
