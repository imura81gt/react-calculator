import React from 'react';

const NumberButton = ({n}) => <button>{n}</button>

export default NumberButton;
